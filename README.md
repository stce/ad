# ad.hpp -- An Operator Overloading Tool

ad.hpp is an operator overloading tool supporting tangent and adjoint modes of AD.
It is realeased under GPL-v3 (see header in src).

## Hello World adjoint example ##
Example for ($`f : \R \rightarrow \R \quad y = x^2`$):
```c++
#include "ad.hpp"
#include <iostream>

using adjoint_t = ad::adjoint_t<double>;
using adjoint   = ad::adjoint<double>;

int main(){
    adjoint::global_tape = adjoint::tape_t::create();
    
    adjoint_t x = 3;
    adjoint::global_tape->register_variable(x);
    
    adjoint_t y = x*x;

    ad::derivative(y) = 1.0;
    adjoint::global_tape->interpret_adjoint();

    std::cout << "f(x) = "      << ad::value(y) 
              << " df/dx(x) = " << ad::derivative(x) 
              << std::endl;

    return 0;
}
```

## Interface Documentation ##

ad.hpp exposes custom (templated) datatypes for which the elemental rules of differentiation are implemented via operator overloading.  
The tangent datatype is called `ad::tangent_t<T>`, the adjoint datatype `ad::adjoint_t<T>`.

## Common interface for tangent and adjoint mode of AD: ##

### ad::value() ###
Returns a reference to the value component of the custom datatype.

```c++
    ad::adjoint_t<double> x;
    ad::value(x) = 42.0;
```

### ad::passive_value() ###
Returns a reference to the base value component of the custom datatype.
For first order differentiation this is the same as ad::value.

```c++
    ad::adjoint_t<ad::tangent_t<double>> x;
    double px = ad::passive_value(x);
```

### ad::derivative() ###
Returns a reference to the derivative component of the custom datatype.
Note, that the derivative is mutable and thus can be modified even for const variables.

```c++
    const ad::adjoint_t<double> x = 42;
    ad::derivative(x) = 1.0;
```

## Adjoint interface: ##
During program execution, data needs to be stored in a data structure called tape.
AD.hpp exposes a global pointer to such a tape called global_tape. This allows to acces this data structure from everywhere in the program.
At startup the tape pointer is uninitialized and must be inititialized with a new tape:
```c++
    ad::adjoint<double>::global_tape = ad::adjoint<double>::tape_t::create();
```
Note the usage of the class adjoint<double> instead of the type adjoint_t<double>.
We call this the adjoint mode, it can also be accessed by using the ad::mode struct.
```c++
    adjoint = ad::mode<adjoint_t<T>>.
```
