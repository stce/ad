#include <iostream>
#include "../../../ad.hpp"

// tangent over adjoint type and mode
using ADt = ad::ga1s<ad::gt1s<double>::type>::type;
using ADm = ad::mode<ADt>;

void foo(typename ADm::external_adjoint_object_t *D){
    ADm::value_t x = D->read_data<ADm::value_t>();
    // handwritten adjoint of y = x^3
    ADm::value_t a1_y = D->get_output_adjoint();
    D->increment_input_adjoint(3*pow(x,2)*a1_y); // increment a1_x
}

void cubed(double x_in){
    ADt x = x_in;

    ADm::global_tape = ADm::tape_t::create();
    ADm::global_tape->register_variable(x);

    ad::derivative(ad::value(x)) = 1.0;
    ADt y = pow(x,3);
    ad::value(ad::derivative(y)) = 1.0;
    
    ADm::global_tape->interpret_adjoint();

    double dx1 = ad::value(ad::derivative(x));
    double dx2 = ad::derivative(ad::value(y));
    double ddx = ad::derivative(ad::derivative(x));
    std::cout << y << " " << dx1 << " " << dx2 << " " << ddx << std::endl;
    ADm::tape_t::remove(ADm::global_tape);
}

void cubed_symbolic_adjoint(double x_in){
    ADt x = x_in;

    ADm::global_tape = ADm::tape_t::create();
    ADm::global_tape->register_variable(x);
    ADm::external_adjoint_object_t* D = ADm::global_tape->create_callback_object<ADm::external_adjoint_object_t>();
    ADm::global_tape->switch_to_passive();

    ad::derivative(ad::value(x)) = 1.0;
    ADm::value_t xp = D->register_input(x);
    D->write_data(x);
    ADm::value_t yp = pow(xp,3);
    ADm::global_tape->switch_to_active();
    ADt y = D->register_output(yp);
    
    ad::value(ad::derivative(y)) = 1.0;
    ADm::global_tape->insert_callback<ADm::external_adjoint_object_t>(foo,D);
    ADm::global_tape->interpret_adjoint();

    double dx1 = ad::value(ad::derivative(x));
    double dx2 = ad::derivative(ad::value(y));
    double ddx = ad::derivative(ad::derivative(x));
    std::cout << y << " " << dx1 << " " << dx2 << " " << ddx << std::endl;
    ADm::tape_t::remove(ADm::global_tape);
}

int main(){
    double x = 4;
    std::cout << "AD "; cubed(x);
    std::cout << "custom adjoint "; cubed_symbolic_adjoint(x);
}