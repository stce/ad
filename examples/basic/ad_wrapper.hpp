#include "ad.hpp"

namespace ad {

  template <typename T>
  using tangent_t = typename gt1s<T>::type;

  template <typename T>
  using adjoint_t = typename ga1s<T>::type;

  template <typename T>
  void init_tape(int tape_size_in_mb=0) { 
    if (mode<T>::global_tape)
      mode<T>::global_tape->reset();
    else if (tape_size_in_mb>0) {
      internal::tape_options o; o.tapesize()=tape_size_in_mb*1024l*1024l/sizeof(T);
      mode<T>::global_tape=mode<T>::tape_t::create(o); 
    } else 
      mode<T>::global_tape=mode<T>::tape_t::create(); 
  }

  template <typename T>
  void add_to_tape(T& x) {
    mode<T>::global_tape->register_variable(x);
  }

  template <typename T>
  void interpret_tape() {
    mode<T>::global_tape->interpret_adjoint();
  }

  template <typename T>
  void remove_tape() { 
      mode<T>::tape_t::remove(mode<T>::global_tape); 
  }

}

