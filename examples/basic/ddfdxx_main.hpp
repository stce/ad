#include <iostream>
#include <cassert>

int main(int argc, char* argv[]) {
  assert(argc==2);
  int n=std::stoi(argv[1]);
  using T=double;
  vector_t<T> x=vector_t<T>::Random(n), dydx(n); 
  matrix_t<T> ddydxx(n,n); 
  T y;
  ddfdxx(x,y,dydx,ddydxx);
  std::cout << y << '\n' << dydx << '\n' << ddydxx << std::endl;
  return 0;
}
