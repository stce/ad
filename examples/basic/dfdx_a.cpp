#include "f_a.hpp"
#include "eigen.hpp"

#include<iostream>
#include<cassert>

template<typename T>
void dfdx(const vector_t<T>& x, T& y, vector_t<T>& dydx) {
  dydx=vector_t<T>::Zero(x.size()); T y_a=1;
  f_a(x,dydx,y,y_a);
}

#include "dfdx_main.hpp"
