#include "eigen.hpp"

#include<iostream>
#include<cassert>

int main(int argc, char* argv[]) {
  assert(argc==2);
  int n=std::stoi(argv[1]);
  using T=double;
  vector_t<T> x=vector_t<T>::Random(n), dydx(n);
  T y;
  dfdx(x,y,dydx);
  std::cout << y << '\n' << dydx << std::endl;
  return 0;
}
