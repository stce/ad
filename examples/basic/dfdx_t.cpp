#include "f_t.hpp"

template<typename T>
void dfdx(const vector_t<T>& x_v, T& y_v, vector_t<T>& dydx) {
  int n=x_v.size();
  vector_t<T> x_t=vector_t<T>::Zero(n); 
  for (int i=0;i<n;i++) {
    x_t(i)=1;
    f_t(x_v,x_t,y_v,dydx(i));
    x_t(i)=0;
  }
}

#include "dfdx_main.hpp"
