#pragma once

#include "eigen.hpp"

template<typename T>
void f(const vector_t<T>& x, T& y) {
  y=exp(sin(x.dot(x)));
}
