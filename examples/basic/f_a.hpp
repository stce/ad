#pragma once

#include "ad_wrapper.hpp"
#include "eigen.hpp"
#include "f.hpp"

template<typename T>
void f_a(const vector_t<T>& x_v, vector_t<T>& x_a, T& y_v, T& y_a) {
  int n=x_v.size();
  vector_t<ad::adjoint_t<T>> x(n); ad::adjoint_t<T> y=0;
  ad::init_tape<ad::adjoint_t<T>>();
  for (int i=0;i<n;i++) {
    x(i)=x_v(i);
    ad::add_to_tape<ad::adjoint_t<T>>(x(i));
  }
  f(x,y);
  y_v=ad::value(y);
  ad::derivative(y)=y_a; y_a=0;
  ad::interpret_tape<ad::adjoint_t<T>>();
  for (int i=0;i<n;i++) x_a(i)+=ad::derivative(x(i));
  ad::remove_tape<ad::adjoint_t<T>>();
}
