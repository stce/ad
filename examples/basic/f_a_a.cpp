#include "ad_wrapper.hpp"
#include "eigen.hpp"
#include "f.hpp"

template<typename T>
void dfdx(const vector_t<T>& x_v, T& y_v, vector_t<T>& dydx) {
  int n=x_v.size();
  vector_t<ad::adjoint_t<T>> x(n); ad::adjoint_t<T> y=0;
  ad::init_tape<ad::adjoint_t<T>>();
  for (int i=0;i<n;i++) {
    x(i)=x_v(i);
    ad::add_to_tape<ad::adjoint_t<T>>(x(i));
  }
  f(x,y);
  y_v=ad::value(y);
  ad::derivative(y)=1;
  ad::interpret_tape<ad::adjoint_t<T>>();
  for (int i=0;i<n;i++) dydx(i)=ad::derivative(x(i));
}

template<typename T>
void ddfdxx(vector_t<T>& xv, T& yv, vector_t<T> &dydx, matrix_t<T> &ddydxx) { 
  int n=xv.size();
  vector_t<ad::adjoint_t<T>> x(n); 
  ad::adjoint_t<T> y;
  for (int i=0;i<n;i++) {
    vector_t<ad::adjoint_t<T>> g(n); 
    ad::init_tape<ad::adjoint_t<T>>();
    for (int j=0;j<n;j++) {
      x(j)=xv(j);
      ad::add_to_tape<ad::adjoint_t<T>>(x(j));
    }
    dfdx(x,y,g);
    ad::derivative(g(i))=1; 
    ad::interpret_tape<ad::adjoint_t<T>>();
    dydx(i)=ad::value(g(i));
    for (int j=0;j<n;j++) ddydxx(j,i)=ad::derivative(x(j));
  }
  yv=ad::value(y);
}

#include "ddfdxx_main.hpp"

