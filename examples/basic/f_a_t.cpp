#include "ad_wrapper.hpp"
#include "eigen.hpp"
#include "f.hpp"

template<typename T>
void dfdx(const vector_t<T>& x_v, T& y_v, vector_t<T>& dydx) {
  size_t n=x_v.size();
  vector_t<ad::adjoint_t<T>> x(n); ad::adjoint_t<T> y=0;
  ad::init_tape<ad::adjoint_t<T>>();
  for (size_t i=0;i<n;i++) {
    x(i)=x_v(i);
    ad::add_to_tape<ad::adjoint_t<T>>(x(i));
  }
  f(x,y);
  y_v=ad::value(y);
  ad::derivative(y)=1;
  ad::interpret_tape<ad::adjoint_t<T>>();
  for (size_t i=0;i<n;i++) dydx(i)=ad::derivative(x(i));
}

template<typename T>
void ddfdxx(const vector_t<T>& x_v, T& y_v, vector_t<T>& dydx_v, matrix_t<T>& ddydxx) {
  size_t n=x_v.size();
  vector_t<ad::tangent_t<T>> x(n), dydx(n); ad::tangent_t<T> y=0;
  for (size_t i=0;i<n;i++) x(i)=x_v(i);
  for (size_t i=0;i<n;i++) {
    ad::derivative(x(i))=1;
    dfdx(x,y,dydx);
    for (size_t j=0;j<n;j++) ddydxx(j,i)=ad::derivative(dydx(j));
    ad::derivative(x(i))=0;
  }
  for (size_t j=0;j<n;j++) dydx_v(j)=ad::value(dydx(j));
  y_v=ad::value(y);
}

#include "ddfdxx_main.hpp"
