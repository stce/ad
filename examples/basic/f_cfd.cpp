#include "eigen.hpp"
#include "f.hpp"
#include <cmath>
#include <limits>

template<typename T>
void dfdx(vector_t<T>& x, T& y, vector_t<T>& dydx) {
  for (int i=0;i<x.size();i++) {
    T dx=fabs(x(i))==0 ? sqrt(std::numeric_limits<T>::epsilon())
       : sqrt(std::numeric_limits<T>::epsilon())*fabs(x(i));
    T yp,ym;
    x(i)+=dx; f(x,yp); x(i)-=2*dx; f(x,ym); x(i)+=dx;
    dydx(i)=(yp-ym)/(2*dx);
  }
  f(x,y);
}

#include "dfdx_main.hpp"
