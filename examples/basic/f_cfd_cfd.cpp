#include "eigen.hpp"
#include "f.hpp"
#include <cmath>
#include <limits>

template<typename T>
void dfdx(vector_t<T>& x, T& y, vector_t<T>& dydx) {
  for (int i=0;i<x.size();i++) {
    T dx=fabs(x(i))==0 ? sqrt(sqrt(std::numeric_limits<T>::epsilon()))
       : sqrt(sqrt(std::numeric_limits<T>::epsilon()))*fabs(x(i));
    T yp,ym;
    x(i)+=dx; f(x,yp); x(i)-=2*dx; f(x,ym); x(i)+=dx;
    dydx(i)=(yp-ym)/(2*dx);
  }
  f(x,y);
}

template<typename T>
void ddfdxx(vector_t<T>& x, T& y, vector_t<T>& dydx, matrix_t<T>& ddydxx) {
  int n=x.size();
  for (int i=0;i<n;i++) {
    T dx=fabs(x(i))==0 ? sqrt(sqrt(std::numeric_limits<T>::epsilon()))
       : sqrt(sqrt(std::numeric_limits<T>::epsilon())*fabs(x(i)));
    T yd;
    vector_t<T> dydxp(n), dydxm(n);
    x(i)+=dx; dfdx(x,yd,dydxp); x(i)-=2*dx; dfdx(x,yd,dydxm); x(i)+=dx;
    for (int j=0;j<n;j++) ddydxx(j,i)=(dydxp(j)-dydxm(j))/(2*dx);
  }
  dfdx(x,y,dydx);
}

#include "ddfdxx_main.hpp"
