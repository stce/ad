#include "ad_wrapper.hpp"
#include "eigen.hpp"
#include "f.hpp"

template<typename T>
void f_t(const vector_t<T>& x_v, const vector_t<T>& x_t, T& y_v, T& y_t) {
  int n=x_v.size();
  vector_t<ad::tangent_t<T>> x(n); ad::tangent_t<T> y=0;
  for (int i=0;i<n;i++) { 
    ad::value(x(i))=x_v(i); 
    ad::derivative(x(i))=x_t(i);
  }
  f(x,y);
  y_v=ad::value(y);
  y_t=ad::derivative(y);
}
