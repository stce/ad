#include "ad_wrapper.hpp"
#include "eigen.hpp"
#include "f.hpp"

template<typename T>
void dfdx(vector_t<T>& xv, T& yv, vector_t<T> &dydx) { 
  int n=xv.size();
  vector_t<ad::tangent_t<T>> x(n); 
  ad::tangent_t<T> y;
  for (int i=0;i<n;i++) {
    for (int j=0;j<n;j++) x(j)=xv(j);
    ad::derivative(x(i))=1; 
    y=yv;
    f(x,y);
    dydx(i)=ad::derivative(y);
  }
  yv=ad::value(y);
}

template<typename T>
void ddfdxx(vector_t<T>& xv, T& yv, vector_t<T> &dydx, matrix_t<T> &ddydxx) { 
  int n=xv.size();
  vector_t<ad::adjoint_t<T>> x(n); 
  ad::adjoint_t<T> y;
  for (int i=0;i<n;i++) {
    vector_t<ad::adjoint_t<T>> g(n); 
    ad::init_tape<ad::adjoint_t<T>>();
    for (int j=0;j<n;j++) {
      x(j)=xv(j);
      ad::add_to_tape<ad::adjoint_t<T>>(x(j));
    }
    y=yv;
    dfdx(x,y,g);
    ad::derivative(g(i))=1; 
    ad::interpret_tape<ad::adjoint_t<T>>();
    dydx(i)=ad::value(g(i));
    for (int j=0;j<n;j++) ddydxx(j,i)=ad::derivative(x(j));
  }
  yv=ad::value(y);
}

#include "ddfdxx_main.hpp"
