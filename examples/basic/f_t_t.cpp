#include "ad_wrapper.hpp"
#include "eigen.hpp"
#include "f.hpp"

template<typename T>
void dfdx(const vector_t<T>& x_v, T& y_v, vector_t<T>& dydx) {
  int n=x_v.size();
  vector_t<ad::tangent_t<T>> x(n); ad::tangent_t<T> y=0;
  for (int i=0;i<n;i++) x(i)=x_v(i);
  for (int i=0;i<n;i++) {
    ad::derivative(x(i))=1;
    f(x,y);
    dydx(i)=ad::derivative(y);
    ad::derivative(x(i))=0;
  }
  y_v=ad::value(y);
}

template<typename T>
void ddfdxx(const vector_t<T>& x_v, T& y_v, vector_t<T>& dydx_v, matrix_t<T>& ddydxx) {
  int n=x_v.size();
  vector_t<ad::tangent_t<T>> x(n), dydx(n); ad::tangent_t<T> y=0;
  for (int i=0;i<n;i++) x(i)=x_v(i);
  for (int i=0;i<n;i++) {
    ad::derivative(x(i))=1;
    dfdx(x,y,dydx);
    for (int j=0;j<n;j++) ddydxx(j,i)=ad::derivative(dydx(j));
    ad::derivative(x(i))=0;
  }
  for (int j=0;j<n;j++) dydx_v(j)=ad::value(dydx(j));
  y_v=ad::value(y);
}

#include "ddfdxx_main.hpp"
